from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from halaman_profile.models import AccountProfile
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .models import Status
from .forms import UpdateStatus_Form

# Create your views here.
response = {'author': 'Kelompok 4 PPW-F'}

def index(request):
    html = 'halaman_forum.html'
    response['company_name'] = request.session.get('company_name')
    response['company_about'] = request.session.get('company_about')
    response['company_type'] = request.session.get('company_type')
    response['company_web'] = request.session.get('company_web')
    response['company_speciality'] = request.session.get('company_speciality')
    response['company_imgURL'] = request.session.get('company_imgURL')
    status = Status.objects.all()
    response['status'] = status
    response['updatestatus_form'] = UpdateStatus_Form
    return render(request, html, response)

def status_post(request):
	form = UpdateStatus_Form(request.POST)
	if request.method == 'POST' and form.is_valid():
		response['status'] = request.POST['status'] #if request.POST['name'] != ""
		status = Status(status=response['status'])
		status.save()
		html = 'halaman_forum.html'
		render(request, html, response)
		return HttpResponseRedirect('/halaman-forum/')
	else:
		return HttpResponseRedirect('/halaman-forum/')
	