from django import forms

class UpdateStatus_Form(forms.Form):
	error_messages = {
		'required': 'Please enter this input',
		'invalid': 'Isi input ini dengan status',
	}
	attrs = {
		'class': 'form-control',
		'placeholder' : "Start writing your job vacancies!",
		'rows' : 6,
		'cols' : 25
	}

	status = forms.CharField(label='', required=True, max_length=500, widget=forms.Textarea(attrs=attrs))
