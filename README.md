[![pipeline status](https://gitlab.com/kelompok4_tugas2_ppw/tugas2-ppw/badges/master/pipeline.svg)](https://gitlab.com/kelompok4_tugas2_ppw/tugas2-ppw/commits/master)
[![coverage report](https://gitlab.com/kelompok4_tugas2_ppw/tugas2-ppw/badges/master/coverage.svg)](https://gitlab.com/kelompok4_tugas2_ppw/tugas2-ppw/commits/master)

Web Design & Programming Project 2
====
## Group 4 (Class F) - 2017/2018 Term 3
Project Members:
1. Izzan Fakhril Islam (1606875806) [@izznfkhrlislm](https://gitlab.com/izznfkhrlislm) 
2. Raihan Mahendra Sutanto (1606917992) [@raihanmhndr](https://gitlab.com/raihanmhndr) 
3. Irfani Ramadianti (1606918295) [@irfanidianti](https://gitlab.com/irfanidianti) 
4. Ihsan Putrananda (1606918276) [@ihsanputra66](https://gitlab.com/ihsanputra66) 

Heroku App Link: ppw-tugas2-4f.herokuapp.com
