from django.test import TestCase
from django.test import Client
from .views import index

# Create your tests here.
class HalamanProfileUnitTest(TestCase):

	def test_halaman_profile_url_is_exist(self):
		response = Client().get('/halaman-profile/')
		self.assertTemplateUsed('/halaman-profile.html/')
		self.assertEqual(response.status_code, 200)
