from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from .models import AccountProfile
from django.http import HttpResponse

# Create your views here.
response = {'author': 'Kelompok 4 PPW-F'}

def index(request):
    html = 'halaman_profile.html'
    response['company_name'] = request.session.get('company_name')
    response['company_about'] = request.session.get('company_about')
    response['company_type'] = request.session.get('company_type')
    response['company_web'] = request.session.get('company_web')
    response['company_speciality'] = request.session.get('company_speciality')
    response['company_imgURL'] = request.session.get('company_imgURL')
    return render(request, html, response)
	