from .models import AccountProfile
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse

@csrf_exempt
def getLoginInfos(request):
	company_name = request.POST.get("company_name")
	company_about = request.POST.get("company_about")
	company_type = request.POST.get("company_type")
	company_web = request.POST.get("company_web")
	company_speciality = request.POST.get("company_speciality")
	company_imgURL = request.POST.get("company_imgURL")
	new_user = AccountProfile(company_name=company_name, company_about=company_about, company_type=company_type, company_web=company_web, company_specialization=company_speciality)
	new_user.save()
	print(company_name)
	print(company_imgURL)
	request.session['company_name'] = company_name
	request.session['company_about'] = company_about
	request.session['company_type'] = company_type
	request.session['company_web'] = company_web
	request.session['company_speciality'] = company_speciality
	request.session['company_imgURL'] = company_imgURL

	return HttpResponse()

@csrf_exempt
def logout(request):
	request.session.flush()
	return HttpResponse()