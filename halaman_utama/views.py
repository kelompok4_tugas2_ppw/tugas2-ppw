from django.shortcuts import render
from django.http import HttpResponseRedirect
from halaman_forum.models import Status
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from response_lowongan.forms import Name_Form, Comment_Form

# Create your views here.
response = {'author': 'Kelompok 4 PPW-F'}

def index(request):
    html = 'halaman_utama.html'
    response['name_form'] = Name_Form
    response['comment_form'] = Comment_Form
    status_job = Status.objects.all().order_by('-created_date')
    paginator = Paginator(status_job, 3)
    page = request.GET.get('page')
    try:
        forum = paginator.page(page)
    except:
        forum = paginator.page(1)

    response['status'] = forum
    
    return render(request, html, response)

