from django.conf.urls import url, include
from .views import index
import response_lowongan.urls as response_lowongan

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^response-lowongan/', include(response_lowongan, namespace='response-lowongan')),
]