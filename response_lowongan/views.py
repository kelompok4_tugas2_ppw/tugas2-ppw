from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from halaman_forum.models import Status
from .models import Comment
from .forms import Name_Form, Comment_Form

# Create your views here.
response = {'author': 'Kelompok 4 PPW-F'}

status_dict ={}

def index(request):
    return HttpResponseRedirect('/halaman-utama/')

def add_comment(request, pk):
	status = Status.objects.get(pk=pk)
	form = Comment_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST['comment']

		comment=Comment(comment=response['comment'])
		comment.status = status
		comment.name = request.POST['name']
		print(comment.name)
		comment.save()
		return redirect('/halaman-utama/')
	else:
		return HttpResponseRedirect('/halaman-utama/')
