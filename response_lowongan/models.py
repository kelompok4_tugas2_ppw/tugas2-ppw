from django.db import models
from halaman_forum.models import Status

# Create your models here.
class Comment(models.Model):
    name = models.TextField(max_length=160, null=True)
    comment = models.TextField(max_length=160)
    created_date = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(Status, null=True, blank=True)

    def __str__(self):
        return self.comment