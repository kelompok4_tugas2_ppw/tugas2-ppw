from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Comment
from halaman_forum.models import Status

# Create your tests here.
class ResponseLowonganUnitTest(TestCase):

	def test_response_lowongan_url_is_exist(self):
		response = Client().get('/response-lowongan/')
		self.assertEqual(response.status_code, 302)
	
	def test_print_message(self):
		status_buatan = Status(status="coba status")
		comment = Comment(name="raihan", comment="ini comment", status=status_buatan)
		self.assertEqual(str(comment), 'ini comment')