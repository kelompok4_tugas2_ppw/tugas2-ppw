"""tugas2ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

import halaman_forum.urls as halaman_forum
import halaman_utama.urls as halaman_utama
import halaman_profile.urls as halaman_profile
import response_lowongan.urls as response_lowongan

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^halaman-utama/', include(halaman_utama, namespace='halaman-utama')),
    url(r'^halaman-forum/', include(halaman_forum, namespace='halaman-forum')),
    url(r'^halaman-profile/', include(halaman_profile, namespace='halaman-profile')),
    url(r'^response-lowongan/', include(response_lowongan, namespace='response-lowongan')),
    url(r'^$', RedirectView.as_view(url='/halaman-utama/', permanent='True'), name='index'),
]
